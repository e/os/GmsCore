package com.android.vending.licensing

import android.content.Context
import android.net.Uri
import android.util.Log
import foundation.e.apps.authdata.AuthDataContract
import org.microg.vending.billing.core.AuthData

class ContentProviderAuthData(
    private val context: Context
) {
    private var authData: AuthData? = null

    fun fetch(): AuthData? {
        Log.i(TAG, "Fetching auth data")

        val cursor = context.contentResolver.query(
            Uri.parse(AUTH_DATA_AUTHORITIES), null, null, null, null
        )

        if (cursor == null || !cursor.moveToFirst()) {
            return null
        }

        authData = AuthData(
            cursor.getString(cursor.getColumnIndexOrThrow(AuthDataContract.EMAIL_KEY)),
            cursor.getString(cursor.getColumnIndexOrThrow(AuthDataContract.AUTH_TOKEN_KEY)),
            cursor.getString(cursor.getColumnIndexOrThrow(AuthDataContract.GSF_ID_KEY)),
            cursor.getString(cursor.getColumnIndexOrThrow(AuthDataContract.CONSISTENCY_TOKEN_KEY)),
            cursor.getString(cursor.getColumnIndexOrThrow(AuthDataContract.DEVICE_CONFIG_TOKEN_KEY)),
            cursor.getString(cursor.getColumnIndexOrThrow(AuthDataContract.EXPERIMENTS_CONFIG_TOKEN_KEY)),
            cursor.getString(cursor.getColumnIndexOrThrow(AuthDataContract.DFE_COOKIE_KEY))
        )

        cursor.close()
        return authData
    }

    companion object {
        const val TAG = "ContentProviderAuthData"
        const val AUTH_DATA_AUTHORITIES = "content://foundation.e.apps.authdata.provider"
    }
}
