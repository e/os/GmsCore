/*
 * SPDX-FileCopyrightText: 2023 microG Project Team
 * SPDX-FileCopyrightText: 2023 E FOUNDATION
 * SPDX-License-Identifier: Apache-2.0
 */

package com.android.vending.splitinstall

interface SplitInstallErrorCode {
    companion object {
        const val API_NOT_AVAILABLE = -5
    }
}
