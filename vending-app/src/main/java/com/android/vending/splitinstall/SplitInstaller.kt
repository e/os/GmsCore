/*
 * SPDX-FileCopyrightText: 2023 microG Project Team
 * SPDX-FileCopyrightText: 2023 E FOUNDATION
 * SPDX-License-Identifier: Apache-2.0
 */

package com.android.vending.splitinstall

import android.content.Context
import com.android.vending.splitinstall.installer.AppLoungeSplitInstaller

interface SplitInstaller {
    fun initialize()
    fun install(packageName: String, moduleName: String)
    fun destroy()
}

enum class SplitInstallerType {
    AppLoungeSplitInstaller
}

object SplitInstallerFactory {
    fun createSplitInstaller(context: Context, type: SplitInstallerType): SplitInstaller {
        when (type) {
            SplitInstallerType.AppLoungeSplitInstaller ->
                return AppLoungeSplitInstaller(context)
            else -> throw IllegalArgumentException("Unknown SplitInstallerType")
        }
    }
}
