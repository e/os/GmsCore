/*
 * SPDX-FileCopyrightText: 2023 microG Project Team
 * SPDX-FileCopyrightText: 2023 E FOUNDATION
 * SPDX-License-Identifier: Apache-2.0
 */

package com.android.vending

import android.app.Service
import android.content.Intent
import android.os.Bundle
import android.os.IBinder
import android.os.Parcel
import android.util.Log
import com.android.vending.splitinstall.SplitInstaller
import com.android.vending.splitinstall.SplitInstallerFactory
import com.android.vending.splitinstall.SplitInstallerType
import com.google.android.play.core.splitinstall.protocol.ISplitInstallService
import com.google.android.play.core.splitinstall.protocol.ISplitInstallServiceCallback

class SplitInstallService : Service() {

    companion object {
        const val TAG = "SplitInstallService"
    }

    private lateinit var mSplitInstaller: SplitInstaller

    override fun onCreate() {
        super.onCreate()

        mSplitInstaller = SplitInstallerFactory.createSplitInstaller(
            applicationContext,
            SplitInstallerType.AppLoungeSplitInstaller
        )

        mSplitInstaller.initialize()
    }

    override fun onBind(p0: Intent?): IBinder {
        return mServiceInterface
    }

    override fun onDestroy() {
        super.onDestroy()
        mSplitInstaller.destroy()
    }

    private var mServiceInterface = object : ISplitInstallService.Stub() {

        override fun a() {
            Log.d(TAG, "a")
        }

        override fun startInstall(
            packageName: String,
            list: List<Bundle>,
            bundle: Bundle,
            callback: ISplitInstallServiceCallback
        ) {
            for (element in list) {

                val apk = element.getString("module_name") ?: element.getString("language")
                apk?.let {
                    mSplitInstaller.install(packageName, apk)
                } ?: logBundleError(element)
            }
        }

        private fun logBundleError(bundle: Bundle) {
            for (entry in bundle.keySet()) {
                Log.e(TAG, "Unknown bundle entry: $entry. Value is ${bundle.get(entry)}")
            }
        }

        override fun c(str: String?) {
            Log.d(TAG, "c")
        }

        override fun d(str: String?) {
            Log.d(TAG, "d")
        }

        override fun e(str: String?) {
            Log.d(TAG, "e")
        }

        override fun getSessionStates(str: String, callback: ISplitInstallServiceCallback) {
            Log.d(TAG, "onGetSessionStates")
            callback.onGetSessionStates(arrayListOf<Bundle>())
        }

        override fun onTransact(code: Int, data: Parcel, reply: Parcel?, flags: Int): Boolean {
            if (super.onTransact(code, data, reply, flags)) return true
            Log.d(TAG, "onTransact [unknown]: $code, $data, $flags")
            return false
        }
    }
}
