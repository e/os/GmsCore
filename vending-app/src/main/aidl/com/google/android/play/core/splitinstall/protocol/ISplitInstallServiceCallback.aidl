/*
 * SPDX-FileCopyrightText: 2016 microG Project Team
 * SPDX-FileCopyrightText: 2023 E FOUNDATION
 * SPDX-License-Identifier: Apache-2.0
 */

package com.google.android.play.core.splitinstall.protocol;

import android.os.Bundle;

interface ISplitInstallServiceCallback {

    // Method not identified yet
    void a();

    void onStartInstall(int i);

    void onCompleteInstall(int i);

    void onCancelInstall(int i);

    void onGetSession(int i);

    void onError(int i);

    void onGetSessionStates(in List<Bundle> list);

    void onDeferredUninstall(in Bundle bundle);

    void onDeferredInstall(in Bundle bundle);

    void onGetSplitsForAppUpdate(int i, in Bundle bundle);

    void onCompleteInstallForAppUpdate();

    void onDeferredLanguageInstall(int i);
}