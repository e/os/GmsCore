/*
 * SPDX-FileCopyrightText: 2016 microG Project Team
 * SPDX-FileCopyrightText: 2023 E FOUNDATION
 * SPDX-License-Identifier: Apache-2.0
 */

package com.google.android.play.core.splitinstall.protocol;

import com.google.android.play.core.splitinstall.protocol.ISplitInstallServiceCallback;

interface ISplitInstallService {

    // Method not identified yet
    void a();

    void startInstall(String str, in List<Bundle> list, in Bundle bundle, in ISplitInstallServiceCallback callback);

    // Method not identified yet
    void c(String str);

    // Method not identified yet
    void d(String str);

    // Method not identified yet
    void e(String str);

    void getSessionStates(String str, in ISplitInstallServiceCallback callback);

}