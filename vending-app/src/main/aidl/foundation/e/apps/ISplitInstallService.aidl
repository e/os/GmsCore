/*
 * SPDX-FileCopyrightText: 2016 microG Project Team
 * SPDX-FileCopyrightText: 2023 E FOUNDATION
 * SPDX-License-Identifier: Apache-2.0
 */

package foundation.e.apps;

interface ISplitInstallService {
    void installSplitModule(String packageName, String moduleName);
}
