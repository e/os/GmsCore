package org.microg.gms.location.manager

import android.content.Context
import android.location.Location
import android.net.Uri
import android.os.Bundle
import com.google.android.gms.location.internal.ClientIdentity

object FakeLocationResolver {
    private val FAKE_LOCATIONS_URI = "content://foundation.e.advancedprivacy.fakelocations"

    private val PARAM_UID = "uid"
    private val PARAM_LATITUDE = "latitude"
    private val PARAM_LONGITUDE = "longitude"

    private data class FakeLocation(var latitude: Double, var longitude: Double)

    fun fakeLocations(
        context: Context,
        identity: ClientIdentity,
        baseLocations: List<Location>
    ): List<Location> {
        val latLon = getFakeLocation(context, identity.packageName, identity.uid)
            ?: return baseLocations

        return baseLocations.map { overrideLatLon(it, latLon) }
    }

    private fun getFakeLocation(context: Context, packageName: String, uid: Int): FakeLocation? {
        return runCatching {
            context.contentResolver.call(
                Uri.parse(FAKE_LOCATIONS_URI),
                "",
                packageName,
                Bundle().apply {
                    putInt(PARAM_UID, uid)
                }
            )?.let {
                FakeLocation(
                    it.getDouble(PARAM_LATITUDE),
                    it.getDouble(PARAM_LONGITUDE)
                )
            }
        }.getOrNull()
    }

    private fun overrideLatLon(baseLocation: Location, latLont: FakeLocation): Location {
        val location = Location(baseLocation)
        location.latitude = latLont.latitude
        location.longitude = latLont.longitude
        location.altitude = 3.0
        location.speed = 0.01f

        return location
    }
}
